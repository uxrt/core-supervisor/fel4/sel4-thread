// Copyright 2022-2024 Andrew Warkentin
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//TODO: add documentation comments

use sel4_sys::seL4_UserContext;
use crate::{
    LocalThread,
    ThreadError,
};

const STACK_ALIGN: usize = 16;

impl LocalThread {
    pub(crate) fn get_stack_align(&self) -> usize {
        STACK_ALIGN
    }
    pub(crate) fn setup_local_user_context(&mut self, ip: usize, arg: usize) -> Result<seL4_UserContext, ThreadError>{
        self.initial_stack_pointer = self.initial_stack_pointer & !(STACK_ALIGN - 1);
        self.initial_stack_pointer -= core::mem::size_of::<usize>();
        Ok(seL4_UserContext {
            rip: ip as usize,
            rsp: self.initial_stack_pointer,
            rflags: 0,
            rax: 0,
            rbx: 0,
            rcx: 0,
            rdx: 0,
            rsi: 0,
            rdi: arg as usize,
            rbp: 0,
            r8: 0,
            r9: 0,
            r10: 0,
            r11: 0,
            r12: 0,
            r13: 0,
            r14: 0,
            r15: 0,
            fs_base: 0,
            gs_base: 0,
        })
    }
}
